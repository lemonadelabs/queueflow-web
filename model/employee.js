class Employee {
    constructor(id, firstname, lastname, employee_code, password, role, enabled, available, slack_channel, escalation) {
        this.id = id;
        this.name = {
            first: firstname,
            last: lastname
        };
        this.employee_code = employee_code;
        this.password = password;
        this.role = role;
        this.enabled = enabled;
        this.available = available;
        this.slack_channel = slack_channel;
        this.escalation = escalation;
    }
}

module.exports = Employee