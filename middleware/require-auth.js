module.exports = function (req, res, next) {

    if (Date.now() <= req.session.SessionMaxAge && req.session.logged_in) {

        // VALID

        req.session.SessionMaxAge = (Date.now() + process.env.SESSION_EXPIRETIME);

        //req.local.user = req.session.username

        next();


    } else {

        req.session.original_url = req.originalUrl;

        res.redirect('/login');
    }

}