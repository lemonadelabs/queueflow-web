var timer;

var assignmentsList = [];

var assignmentIDToEdit = {};
var editAssignmentEmployeeID;

var available_empl = [];


$(document).ready( function () {

    $('*[data-make-assignment]').click( function () {
        let assignmentType = $(this).attr("data-make-assignment");
        let assignmentHumanReadable = $(this).text();
        $("*[data-fill-assignmenthr]").text(assignmentHumanReadable)
        $("#assignment-type-short").val(assignmentType);
        $("#assignmentButtons").hide();
        $("#assignmentDetails").show();
        if (assignmentType === "escalation") {
            $("#assignmentDetails-escalatie").show();
            $("#num-customers-listitem").hide();
            $("#assignment-num-customers").val(0);
            if (localStorage.getItem('employee-id') !== null ) {
                let optionToSelect = localStorage.getItem('employee-id') ;
                //Change the #countries select element to the Ireland option.
                $('#assignment-escalation-empl').val(optionToSelect);
            }
        }
        setTimeout(function() {
            $('#assignment-num-customers').focus()
        }, 1000);
    });

    $("#assignment-num-customers").on('change', function () {
        let max = $(this).attr("max");
        if ($(this).val().length > max ) {
            $(this).addClass("red-text");
            return false
        } else {
            $(this).removeClass("red-text");
        }
    });

    $("#send-assignment").click( function () {
        let num_customers = $("#assignment-num-customers").val();
        let a_type_hr = $("#assignment_type").text();
        let a_type = $("#assignment-type-short").val();
        let escalatie_to_employee = $("#assignment-escalation-empl").val();

        if ( a_type !== "escalation" ) {

            if (num_customers.length < 1) {
                popup.make({
                    title: "Vul een waarde in!",
                    data: "Vul een waarde in voor het aantal klanten.",
                    callbackFunction: function () {
                        popup.destroy();
                        setTimeout(function() {
                            $('#assignment-num-customers').focus()
                        }, 500);
                        return
                    }
                });
                return
            }

            if (num_customers < 1 || num_customers > 2) {
                popup.make({
                    title: "Waarde is ongeldig",
                    data: "Het minimale aantal klanten is 1 en het maximale is 2.",
                    callbackFunction: function () {
                        popup.destroy();
                        setTimeout(function () {
                            $('#assignment-num-customers').focus()
                        }, 500);
                        return
                    }
                });
                return
            }

            escalatie_to_employee = null;
        }

        socket.emit('new_assignment', {type: a_type.trim(), type_hr: a_type_hr.trim(), num_customers: num_customers, state: "Wachten", emp_id: null, escalation_to_employee: escalatie_to_employee });
        $('#assignment-num-customers').val("");
        $("#assignmentDetails").hide();
        $("#assignmentButtons").show();
        $("#assignmentDetails-escalatie").hide();
        $("#num-customers-listitem").show();
    });

    $("#cancel-create-assignment").click( function () {

        $('#assignment-num-customers').val("");
        $("#assignmentDetails").hide();
        $("#assignmentButtons").show();
        $("#assignmentDetails-escalatie").hide();
        $("#num-customers-listitem").show();

    });

    socket.on('load_assignments', function(assignmentData){
        assignmentsList = assignmentData;
        /*console.log("assignmentData")
        console.log(assignmentData)
        console.log("assignmentsList")
        console.log(assignmentsList)*/
        $("#assignment-queue").html("");
        for (let i = 0; i < assignmentData.length; i++) {

            let ionicon;
            let color;

            switch(assignmentData[i].state) {
                case "Wachten":
                    // code block
                    ionicon = "time-outline";
                    color = "orange-text";
                    break;
                default:
                    ionicon = "checkmark-circle-outline";
                    color = "green-text";
                // code block

            }

            $("#assignment-queue").append(`<div class="list-item" data-assignment-id="`+assignmentData[i].position+`" data-assignment-type="`+assignmentData[i].type+`">
                <p>`+assignmentData[i].type_hr+`</p>
                <p class="subtitle">`+assignmentData[i].num_customers+` klanten</p>
                <p class="value">`+assignmentData[i].state+`&nbsp;&nbsp;&nbsp;<ion-icon name="`+ionicon+`" class="`+color+`"></ion-icon></p>
            </div>`);

            let employee_id = Number(localStorage.getItem("employee-id"));

            if (localStorage.getItem("employee-id") !== null && Number(employee_id) !== 0) {
                $.get( "/api/v1/employee/got/assignment/"+employee_id,
                    {  }
                ).done(function( data ) {

                    if ('type' in data) {
                        // Employee got an assignment
                        console.log(Number(assignmentData[i].emp_id))
                        console.log(Number(localStorage.getItem("employee-id")))
                        console.log(Number(assignmentData[i].emp_id) === Number(localStorage.getItem("employee-id")))
                        if (Number(assignmentData[i].emp_id) === Number(localStorage.getItem("employee-id"))) {
                            $("#wait-for-assignment, #logout-employee").hide();
                            $("#end-assignment").show();
                            //alert("Yes, you are the person that sould see the details")
                            // Fill sales tab details
                            $("*[data-sales-assignment-type-of-sale]").text(assignmentData[i].type_hr)
                            $("*[data-sales-assignment-id]").text(assignmentData[i].position)
                            $("*[data-sales-assignment-employee-state]").text("Bezig").removeClass("green-text").addClass("red-text");
                            $("*[data-sales-assignment-num-customers]").text(assignmentData[i].num_customers)

                            if (assignmentData[i].type === "escalation") {
                                //alert("Assignment is an escalation")
                                // shwo listitme with person requested the escalation
                                $("*[data-sales-assignment-requested-escalation]").text(assignmentData[i].escalation_to_employee).parent().show();
                            } else {
                                $("*[data-sales-assignment-requested-escalation]").text("-").parent().hide();
                            }


                        } else {
                            //alert("OOOPS \n"+assignmentData[i].emp_id+"\n"+localStorage.getItem("employee-id"))

                        }
                        console.log("Employee got an assignment");
                    } else {
                        // Employee does not have a running assignment
                        console.log("Employee does not have a running assignment");


                    }

                });
            }






        }

    });

    socket.on('new_assignment', function(assignmentData){
        let ionicon;
        let color;

        switch(assignmentData.state) {
            case "Wachten":
                // code block
                ionicon = "time-outline";
                color = "orange-text";
                break;
            default:
                ionicon = "checkmark-circle-outline";
                color = "green-text";
            // code block

        }
        $("#assignment-queue").append(`<div class="list-item" data-assignment-id="`+assignmentData.position+`" data-assignment-type="`+assignmentData.type+`">
                <p>`+assignmentData.type_hr+`</p>
                <p class="subtitle">`+assignmentData.num_customers+` klanten</p>
                <p class="value">`+assignmentData.state+`&nbsp;&nbsp;&nbsp;<ion-icon name="`+ionicon+`" class="`+color+`"></ion-icon></p>
            </div>`);
    });

});

$(document).on('click', '.peek-pop-layer', function() {
    // This will work!
    $(".peek-pop-layer").fadeOut(125).remove();
    $(".peek-pop").hide().remove();
});

$(document).on('click', '#update-assignment', function() {

    let new_employeeID = $("#aae-ea").val();

    if (new_employeeID !== null || new_employeeID !== undefined) {
        let assignment_data = $.grep(assignmentsList, function(e) { return e.position == assignmentIDToEdit });

        console.log("assignment_data"+JSON.stringify(assignment_data))
        console.log("assignment_data.emp_id"+assignment_data[0].emp_id)


        assignment_data[0].emp_id = new_employeeID;
        let name_old_emp = assignment_data[0].state;

        let old_emp_avail = true;

        let popupmsg = "";

        alert("[DEBUG] before prompt available");

        let r = confirm(name_old_emp+" Beschikbaar zetten?");
        if (r == true) {
            socket.emit('employee_state', {"employee_id": editAssignmentEmployeeID, "available": 1});
            popupmsg = "Taak is gewisseld! Originele werknemer is niet meer beschikbaar.";

        } else {
            popupmsg = "Taak is gewisseld! Originele werknemer is nu weer beschikbaar.";
        }

        //alert("new_employeeID "+new_employeeID)
        //State should be changed in backend
        socket.emit('employee_state', {"employee_id": new_employeeID, "available": 0});
        socket.emit('update_assignment', assignment_data);

        popup.make({
            title: "Gelukt!",
            data: popupmsg,
            showCancelButton: false,
            callbackFunction: function () {
                popup.destroy();
                // hide the blur popup
                $(".peek-pop-layer").fadeOut(125).remove();
                $(".peek-pop").hide().remove();
            }
        });
    } else {
        popup.make({
            title: "Bummer!",
            data: "Toewijzen niet gelukt. Probeer het opnieuw, of kies andere werknemer",
            showCancelButton: false,
            callbackFunction: function () {
                popup.destroy();
            }
        });
    }



});

$(document).on('click', '#delete-assignment', function() {
    // This will work!

        popup.make({
            title: "Weet je het zeker?",
            data: "Oude employee "+editAssignmentEmployeeID+"",
            showCancelButton: true,
            cancelButtonText: "Cancel",
            callbackFunction: function () {
                popup.destroy();
                // Do shit

                let emp_idOBJ = $.grep(assignmentsList, function(e) { return e.position === assignmentIDToEdit });

                console.log("emp_idOBJ "+JSON.stringify(emp_idOBJ))
                console.log("assignmentIDToEdit "+assignmentIDToEdit)



                socket.emit('remove_assignment', {"assignment_position": assignmentIDToEdit});

                if (editAssignmentEmployeeID !== null) {

                    let name_old_emp = assignment_data[0].state;

                    let old_emp_avail = true;

                    let popupmsg = "";

                    alert("[DEBUG] before prompt available");

                    let r = confirm(name_old_emp+" Beschikbaar zetten?");
                    if (r == true) {
                        socket.emit('employee_state', {"employee_id": editAssignmentEmployeeID, "available": 1});
                        popupmsg = "Taak is verwijderd! Originele werknemer is niet meer beschikbaar.";

                    } else {
                        popupmsg = "Taak is verwijderd! Originele werknemer is nu weer beschikbaar.";
                    }


                    popup.make({
                        title: "Gelukt!",
                        data: popupmsg,
                        showCancelButton: false,
                        callbackFunction: function () {
                            popup.destroy();
                        }
                    });

                } else {
                    popup.make({
                        title: "Gelukt!",
                        data: "De taak is verwijderd.",
                        showCancelButton: false,
                        callbackFunction: function () {
                            popup.destroy();
                        }
                    });
                }


            }
        });

    $(".peek-pop-layer").fadeOut(125).remove();
    $(".peek-pop").hide().remove();
});

$(document).on('mousedown touchstart', '#assignment-queue > .list-item', function(e) {
    // This will work!

    if ($(e.target).attr("data-assignment-id") === undefined) {
        assignmentIDToEdit = $(e.target).parent("div").attr("data-assignment-id")
    } else {
        assignmentIDToEdit = $(e.target).attr("data-assignment-id")
    }

    assignmentToEdit = $.grep(assignmentsList, function(e) { return e.position === assignmentIDToEdit });


    editAssignmentEmployeeID = assignmentIDToEdit.emp_id;
    //alert(editAssignmentEmployeeID)

    timer = setTimeout(function(){

        assignmentToEdit = $.grep(assignmentsList, function(e) { return e.position == assignmentIDToEdit });

        console.log(assignmentToEdit)

        // Show the peek and pop edit functionality

        //alert(assignmentIDToEdit);

        let ionicon;
        let color;

        let assignmentToEditTimeOut = assignmentToEdit;

        console.log(assignmentToEdit)
        console.log(assignmentToEditTimeOut)
        console.log(assignmentToEditTimeOut[0].state)
        editAssignmentEmployeeID = assignmentToEditTimeOut[0].emp_id;

        switch(assignmentToEditTimeOut[0].state) {
            case "Wachten":
                // code block
                ionicon = "time-outline";
                color = "orange-text";
                break;
            default:
                ionicon = "checkmark-circle-outline";
                color = "green-text";
            // code block

        }

        $("body").append(`<div class="peek-pop-layer"></div>
<div class="peek-pop">
<h3>PAS ASSIGNMENT AAN</h3>
<div class="list-container section">

    <div class="list-item">
        <p>Assignment ID</p>
        <p class="value">`+assignmentIDToEdit+`</p>
    </div>

    <div class="list-item">
        <p>Type</p>
        <p class="value">`+assignmentToEditTimeOut[0].type_hr+`</p>
    </div>

    <div class="list-item">
        <p>Status / werknemer</p>
        <p class="value">`+assignmentToEditTimeOut[0].state+`&nbsp;&nbsp;&nbsp;<ion-icon name="`+ionicon+`" class="`+color+`"></ion-icon></p>
    </div>
    
    <div class="list-item">
        <p>Wijs handmatig toe</p>
        <p class="value"><select id="aae-ea">
    <option selected disabled>Bezig met ophalen...</option>
</select></p>
    </div>

</div>
<div class="button" id="update-assignment"><h3>Sla wijzigingen op</h3></div>
<div class="button red-background" id="delete-assignment"><h3>Verwijder</h3></div>
</div>
`);

        $(".peek-pop-layer").fadeIn(250);

        $.get( "/api/v1/employee/get/available/all",
            {  }
        ).done(function( data ) {
            $("#aae-ea").html("");
            let d = data;
            if (data.length > 0) {
                available_empl = data;
                $("#aae-ea").append('<option selected disabled>-- Maak een keuze --</option>');
                for (let i = 0; i < d.length; i++) {
                    $("#aae-ea").append('<option value="'+d[i].id+'">'+d[i].name.first+' '+d[i].name.last+'</option>');
                }
            } else {
                $("#aae-ea").html('<option disabled selected>-- Geen beschikbaar --</option>');
            }

        });





    },0.75*1000);
});

$(document).on('mouseup mouseleave touchend', '#assignment-queue > .list-item', function() {
    clearTimeout(timer);
});

function bindPeekPopAssignment() {

}

function getAssignmentToEditFunc () {
    for(var i = 0, assignmentToEdit = null; i < assignmentsList.length; ++i) {
        if(assignmentsList[i].id !== assignmentIDToEdit)
            continue;
        return assignmentsList[i];
        console.log(assignmentToEdit)
        break;
    }
}