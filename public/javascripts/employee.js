var maySetAvailable = true;

$(document).ready( function () {


    if (localStorage.getItem("employee-id") !== null) {

        let employeeeID = localStorage.getItem("employee-id");
        $.get( "/api/v1/employee/get/available/"+employeeeID,
            {  }
        ).done(function( data ) {
            console.log("Availability for userid "+employeeeID+": "+ data)
            if (data.available !== 1) {
                // Logout
                localStorage.removeItem('employee-name');
                localStorage.removeItem('employee-id');
                $("#sales-select").show();
                $("#sales-active").hide();
            } else {
                let employee_firstname = localStorage.getItem('employee-name');
                $("*[data-fill-employee-firstname]").text(employee_firstname);
                $("#sales-select").hide();
                $("#sales-active").show();
            }
        });


    }

    socket.on('available_employees', function(employeesarray) {

        console.log(employeesarray);
        $("#available_empl_list").html("");
        for (let i = 0; i < employeesarray.length; i++) {

            $("#available_empl_list").append(`<div class="list-item" data-employee-id="`+employeesarray[i].id+`">
                    <p>` + employeesarray[i].name.first + ` ` + employeesarray[i].name.last + `</p>
                </div>`);
        }

    });

    socket.on('employee_state', function(msg){

        //alert(JSON.stringify(msg));
        let employee_id_logged = localStorage.getItem('employee-id');
        if (employee_id_logged !== null || employee_id_logged !== undefined) {
            $.get("/api/v1/employee/got/assignment/" + employee_id_logged,
                {}
            ).done(function (data) {

                if ('type' in data) {
                    // Employee got an assignment
                    maySetAvailable = false;
                    //alert("This employee got an assignment")
                } else {
                    // Employee does not have a running assignment
                    $("*[data-sales-assignment-type-of-sale]").text("-")
                    $("*[data-sales-assignment-id]").text("-")
                    $("*[data-sales-assignment-requested-escalation]").hide();
                    $("#waiting-msg").text("Wachten op assignment...");
                    $("*[data-sales-assignment-num-customers]").text("-")
                    $("#wait-for-assignment, #logout-employee").show();
                    $("#end-assignment").hide();
                    $("*[data-sales-assignment-employee-state]").text("Beschikbaar").removeClass("orange-text").removeClass("red-text").addClass("green-text").removeClass("blue-text");
                    $("#sales-select").hide();
                    $("#sales-active").show();
                }

            });

            if (employee_id_logged === msg.emp_id && msg.available === 1) {
                // Employee does not have a running assignment
                $("*[data-sales-assignment-type-of-sale]").text("-")
                $("*[data-sales-assignment-id]").text("-")
                $("*[data-sales-assignment-requested-escalation]").hide();
                $("#waiting-msg").text("Wachten op assignment...");
                $("*[data-sales-assignment-num-customers]").text("-")
                $("#wait-for-assignment, #logout-employee").show();
                $("#end-assignment").hide();
                $("*[data-sales-assignment-employee-state]").text("Beschikbaar").removeClass("orange-text").removeClass("red-text").addClass("green-text").removeClass("blue-text");
                $("#sales-select").hide();
                $("#sales-active").show();
            }

        }

    });

    socket.on('remove_assignment', function(msg){

        console.log("checking remove");
        console.log($("*[data-sales-assignment-id]").text());
        console.log(msg.assignment_position);
        if ($("*[data-sales-assignment-id]").text() === msg.assignment_position) {

            // Set employee to available
            $("*[data-sales-assignment-type-of-sale]").text("-")
            $("*[data-sales-assignment-id]").text("-")
            $("*[data-sales-assignment-requested-escalation]").hide();
            $("#waiting-msg").text("Wachten op assignment...");
            $("*[data-sales-assignment-num-customers]").text("-")
            $("#wait-for-assignment, #logout-employee").show();
            $("#end-assignment").hide();
            $("*[data-sales-assignment-employee-state]").text("Beschikbaar").removeClass("orange-text").removeClass("red-text").addClass("green-text").removeClass("blue-text");
            $("#sales-select").hide();
            $("#sales-active").show();
        }

        //alert(JSON.stringify(msg));
        let employee_id_logged = localStorage.getItem('employee-id');
        $.get( "/api/v1/employee/got/assignment/"+employee_id_logged,
            {  }
        ).done(function( data ) {

            if ('type' in data) {
                // Employee got an assignment
                maySetAvailable = false;
                //alert("This employee got an assignment")
            } else {
                // Employee does not have a running assignment
                $("*[data-sales-assignment-type-of-sale]").text("-")
                $("*[data-sales-assignment-id]").text("-")
                $("*[data-sales-assignment-requested-escalation]").hide();
                $("#waiting-msg").text("Wachten op assignment...");
                $("*[data-sales-assignment-num-customers]").text("-")
                $("#wait-for-assignment, #logout-employee").show();
                $("#end-assignment").hide();
                $("*[data-sales-assignment-employee-state]").text("Beschikbaar").removeClass("orange-text").removeClass("red-text").addClass("green-text").removeClass("blue-text");
                $("#sales-select").hide();
                $("#sales-active").show();
            }

        });

        if (employee_id_logged === msg.emp_id && msg.available === 1) {

        }

    });

    $("*[data-select-employee]").click( function () {
        let employee_firstname = $(this).attr("data-employee-firstname");
        let employee_id = $(this).attr("data-employee-id");
        localStorage.setItem('employee-name', employee_firstname);
        localStorage.setItem('employee-id', employee_id);
        $("*[data-fill-employee-firstname]").text(employee_firstname)


        $.get( "/api/v1/employee/got/assignment/"+employee_id,
            {  }
        ).done(function( data ) {

            if ('type' in data) {
                // Employee got an assignment
                maySetAvailable = false;
                $("*[data-sales-assignment-employee-state]").text("Ophalen").removeClass("orange-text").removeClass("red-text").removeClass("green-text").addClass("blue-text");
                $("#sales-select").hide();
                $("#sales-active").show();
                //alert("This employee got an assignment")
            } else {
                // Employee does not have a running assignment
                socket.emit('employee_state', {"employee_id": employee_id, "available": 1})
                $("*[data-sales-assignment-employee-state]").text("Beschikbaar").removeClass("orange-text").removeClass("red-text").addClass("green-text");
                $("#sales-select").hide();
                $("#sales-active").show();
            }

        });

    });

    $("#logout-employee").click( function () {
        popup.make({
            title: "Weet je het zeker?",
            data: "Je bent niet meer beschikbaar voor de winkelvloer.",
            showCancelButton: true,
            cancelButtonText: "Cancel",
            callbackFunction: function () {
                maySetAvailable = false;
                localStorage.setItem('maySetAvailable', "0");
                popup.destroy();
                let empid = localStorage.getItem("employee-id")
                $.post( "/api/v1/employee/change/available/"+empid+"/0",
                    {  }
                ).done(function( data ) {
                    if (data.affectedRows === 1) {
                        let employee_firstname = localStorage.getItem('employee-name');
                        localStorage.removeItem('employee-name');
                        localStorage.removeItem('employee-id');
                        $("#sales-select").show();
                        $("#sales-active").hide();
                    } else {
                        let empid = localStorage.getItem("employee-id")
                        $.get( "/api/v1/employee/get/available/"+empid,
                            {  }
                        ).done(function( data ) {

                            if (data.available === 0) {
                                localStorage.removeItem('employee-name');
                                localStorage.removeItem('employee-id');
                                $("#sales-select").show();
                                $("#sales-active").hide();
                                $("#set-available-employee").hide();
                            } else {
                                popup.make({
                                    title: "Afmelden niet gelukt",
                                    data: "Mogelijk ben je al weer aangemeld. Probeer het nogmaals of reset de app.",
                                    showCancelButton: false,
                                    callbackFunction: function () {
                                        popup.destroy();
                                    }
                                });
                            }

                        });

                    }
                });

            }
        });
    });

    //TODO add set available
    $("#set-available-employee").click( function () {
        let employee_id = localStorage.getItem("employee-id");
        socket.emit('employee_state', {"employee_id": employee_id, "available": 1});
        $("#set-available-employee").hide();
    });

    $("#end-assignment").click( function () {
        popup.make({
            title: "Weet je het zeker?",
            data: "Beëindig deze assignment alleen als je transactie hebt afgerond.",
            showCancelButton: true,
            cancelButtonText: "Cancel",
            callbackFunction: function () {
                popup.destroy();
                // Do shit
                console.log("Employee ended assignment, maySetAvailable:"+maySetAvailable)
                maySetAvailable = true;
                console.log("Updated maySetAvailable to true, maySetAvailable:"+maySetAvailable)
                let employee_id = localStorage.getItem("employee-id");
                let assignment_position = $("*[data-sales-assignment-id]").text();
                socket.emit('end_assignment', {"emp_id": employee_id, "assignment_position": assignment_position});

                $("#wait-for-assignment, #logout-employee").show();
                $("#end-assignment").hide();
                //alert("Yes, you are the person that sould see the details")
                // Fill sales tab details
                $("*[data-sales-assignment-type-of-sale]").text("-")
                $("*[data-sales-assignment-id]").text("-")
                $("*[data-sales-assignment-requested-escalation]").hide();
                $("*[data-sales-assignment-employee-state]").text("Afronden").removeClass("red-text").addClass("orange-text");
                $("#wait-for-assignment").hide();
                $("#set-available-employee").show();
                $("*[data-sales-assignment-num-customers]").text("-")

            }
        });

    });


    function setEmployeeAvailable( employee_id ) {

        console.log("Called setEmployeeAvailable function, maySetAvailable:"+maySetAvailable)

        if (maySetAvailable) {

            console.log("MaySetAvailable check succeeded, maySetAvailable:"+maySetAvailable)

            let employee_id_logged = localStorage.getItem('employee-id');
            $.get( "/api/v1/employee/got/assignment/"+employee_id_logged,
                {  }
            ).done(function( data ) {

                if ('type' in data) {
                    // Employee got an assignment
                    //alert("This employee got an assignment")
                    console.log("Employee got assignment, maySetAvailable:"+maySetAvailable)
                    maySetAvailable = false;
                    console.log("Updated maySetAvailable to false, maySetAvailable:"+maySetAvailable)
                } else {
                    // Employee does not have a running assignment

                    $.post( "/api/v1/employee/change/available/"+employee_id+"/1",
                        {  }
                    ).done(function( data ) {
                        if (data.affectedRows === 1) {
                            console.log("Employee set to available, maySetAvailable:"+maySetAvailable)
                            $("*[data-sales-assignment-type-of-sale]").text("-")
                            $("*[data-sales-assignment-id]").text("-")
                            $("*[data-sales-assignment-requested-escalation]").hide();
                            $("#waiting-msg").text("Wachten op assignment...");
                            $("*[data-sales-assignment-num-customers]").text("-")
                            $("#wait-for-assignment, #logout-employee").show();
                            $("#end-assignment").hide();
                            $("*[data-sales-assignment-employee-state]").text("Beschikbaar").removeClass("orange-text").removeClass("red-text").addClass("green-text").removeClass("blue-text");
                            $("#sales-select").hide();
                            $("#sales-active").show();
                        } else {
                            console.error("Data [/api/v1/employee/change/available/"+employee_id+"/1]: "+data);
                            popup.make({
                                title: "Er is iets mis gegaan",
                                data: "Aanmelden is niet gelukt. Meld je opnieuw aan.",
                                showCancelButton: false,
                                callbackFunction: function () {
                                    popup.destroy();
                                }
                            });
                        }
                    });
                }

            });



            //macbook-pro-tb-van-Bas.local:3000/api/v1/employee/change/available/1/0

        }


    }

});