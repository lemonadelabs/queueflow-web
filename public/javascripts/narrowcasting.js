

socket.on('load_assignments', function(assignmentData){
    $("#assignment-queue").html("");
    for (let i = 0; i < assignmentData.length; i++) {

        let ionicon;
        let color;

        switch(assignmentData[i].state) {
            case "Wachten":
                // code block
                ionicon = "time-outline";
                color = "orange-text";
                break;
            default:
                ionicon = "checkmark-circle-outline";
                color = "green-text";
            // code block

        }

        $("#assignment-queue").append(`<div class="list-item"  data-assignment-id="`+assignmentData[i].position+`" data-assignment-type="`+assignmentData[i].type+`">
                <p>`+assignmentData[i].type_hr+`</p>
                <p class="subtitle">`+assignmentData[i].num_customers+` klanten</p>
                <p class="value">`+assignmentData[i].state+`&nbsp;&nbsp;&nbsp;<ion-icon name="`+ionicon+`" class="`+color+`"></ion-icon></p>
            </div>`);
    }

});

socket.on('available_employees', function(employeesarray) {

    console.log(employeesarray);
    $("#available_empl_list").html("");
    for (let i = 0; i < employeesarray.length; i++) {

        $("#available_empl_list").append(`<div class="list-item">
                    <p>` + employeesarray[i].name.first + ` ` + employeesarray[i].name.last + `</p>
                </div>`);
    }

});

socket.on('new_assignment', function(assignmentData){
        let ionicon;
        let color;

        switch(assignmentData.state) {
            case "Wachten":
                // code block
                ionicon = "time-outline";
                color = "orange-text";
                break;
            default:
                ionicon = "checkmark-circle-outline";
                color = "green-text";
            // code block

        }
        $("#assignment-queue").append(`<div class="list-item" data-assignment-id="`+assignmentData.position+`" data-assignment-type="`+assignmentData.type+`">
                <p>`+assignmentData.type_hr+`</p>
                <p class="subtitle">`+assignmentData.num_customers+` klanten</p>
                <p class="value">`+assignmentData.state+`&nbsp;&nbsp;&nbsp;<ion-icon name="`+ionicon+`" class="`+color+`"></ion-icon></p>
            </div>`);
    });
