// Copyright 2020 Bas Kasemir

var appName = "Queue Flow";
var appVersion = "1.4.2"
var appBuild = "(Release Candidate)"
var profile = "dev"; // 'prod' or 'dev'

var isPWA = false;

$( document ).ready( function () {
    if (localStorage.getItem("version") !== null) {
        localStorage.setItem('version', appVersion);
    }

    $('*[data-fill-app-version="number"]').text(appVersion);
    $('*[data-fill-app-version="build"]').text(appBuild);

    $(function() {
        $(document).on("touchmove", function(evt) {
            if ($("body").hasClass("disablescroll")) {
                evt.preventDefault();
            }
        });
    });

    document.ontouchmove = function(event){
        if ($("body").hasClass("disablescroll")) {
            event.preventDefault();
        }
    }

    $("#reset-ls").click(function () {
        popup.make({
            title: "Weet je het zeker?",
            data: "Dit reset de app op jouw device.",
            showCancelButton: true,
            callbackFunction: function () {
                popup.destroy();
                localStorage.clear();
                window.location.reload();
            }
        });
    });

    $('*[data-prompt-open]').click( function () {
        $("body").addClass("disablescroll");
        let selector = '.prompt-message[data-prompt-name="'+$(this).attr("data-prompt-open")+'"]';
        $(selector).show();
    });

    $('*[data-prompt-action="close"]').click( function () {
        $("body").removeClass("disablescroll");
        $(this).parent().parent().hide();
    });

    if (localStorage.getItem("accept-tou") === null || localStorage.getItem("accept-tou") === "false" || localStorage.getItem("version")) {

        $("body").addClass("disablescroll");
        $('.prompt-message[data-prompt-name="first-run"]').show();
        $('*[data-tou-action]').click( function () {
            let action = $(this).attr('data-tou-action');
            if (action === "true") {
                let timeStamp = Math.floor(Date.now() / 1000);
                localStorage.setItem("accept-tou", "true");
                localStorage.setItem("accept-date", timeStamp.toString());
                $("body").removeClass("disablescroll");
                $(this).parent().parent().hide().remove();
            } else {
                $("body").removeClass("disablescroll")
                $("head").html('<meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0"><title>Queue Flow</title>');
                $("body").html('<iframe src="https://baskasemir.basmonkey.nl" style="height: 100vh; width: 100vw; border: none; position: fixed; top: 0; left: 0;">Your Browser does not want to view this page :(</iframe>')
                //window.location = "itms-apps://";
                //window.location = "safari://";
                //window.location = "https://baskasemir.basmonkey.nl";
                //window.location = "prefs://"
            }
        });


        //                localStorage.setItem("first-run", false)
    }

    console.log(appName + " " + appVersion);
});