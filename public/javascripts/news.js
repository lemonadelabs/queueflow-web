var vid;
var vidkeys = [];

$( document ).ready( function () {

    fetchNews();
    bindNewsTouch();
    playVid();

    $('*[data-refresh="newslist"]').click( function () {
        fetchNews();
        bindNewsTouch();
    });

});

function fetchNews() {
    $(".news-list").empty();
    let newsData = (function () {
        let json = null;
        $.ajax({
            'async': false,
            'global': false,
            'url': "data/news.json",
            'dataType': "json",
            'success': function (data) {
                json = data;
            }
        });
        return json;
    })();

    function decodeUnicode(str) {
        // Going backwards: from bytestream, to percent-encoding, to original string.
        return decodeURIComponent(atob(str).split('').map(function (c) {
            return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
        }).join(''));
    }

    function encodeUnicode(str) {
        // first we use encodeURIComponent to get percent-encoded UTF-8,
        // then we convert the percent encodings into raw bytes which
        // can be fed into btoa.
        return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g,
            function toSolidBytes(match, p1) {
                return String.fromCharCode('0x' + p1);
            }));
    }

    newsData.forEach( function (obj, key) {

        let newsCard = `<div class="news-card">
            <ion-icon class="close-news-card hideonload" name="close-circle"></ion-icon>`;

        let posterHTML = "";

        let data = atob(obj.data);

        if (obj.postertype === "img") {
            posterHTML = `<div class="posterimage" style="background-image: url( '` + obj.posterurl + `' );"></div>`;
        } else if (obj.postertype === "mov") {
            posterHTML = '<video id="vid' + key + '" class="posterimage newsvid" width="320" height="240" autoplay controls muted><source src="' + obj.posterurl + '" type="video/mp4">Your browser does not support the video tag.</video>';
            vidkeys.push("vid"+key);
        }

        newsCard = newsCard + posterHTML + `
        
        <div class="content">
            <div class="head">
            <h2>` + obj.title + `</h2>
        <p>` + obj.subtitle + `</p>
        </div>
        <div class="data hideonload">
            ` + decodeUnicode(obj.data) + `
        </div>
        </div>
        </div>
        
        `;

        $(".news-list").append(newsCard);
        bindNewsTouch();
    });

    setTimeout(function () {
        playVid();
    }, 5000);
}

function playVid() {

    console.log(vidkeys);
    for (let i = 0; i < vidkeys.length; i++) {
        console.log(vidkeys[i]);
        let id = vidkeys[i].toString();
        vid = document.getElementById(id);
        console.log(vid);
        vid.play();
    }
}

function bindNewsTouch() {
    $(".news-card:not(.opened)").click( function () {
        $(this).addClass("opened").children(".content").children(".data").show();
        $(this).children(".close-news-card").show();
    });

    $(".close-news-card").click( function (e) {
        e.stopPropagation();
        $(this).hide();
        $(this).siblings(".content").children(".data").hide();
        $(this).parent(".opened").removeClass("opened");
    });
}