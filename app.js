const fs = require('fs');
require('dotenv').config({path: __dirname + '/.env'})
var express = require('express');
const app = express(); // the main app
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const helmet = require('helmet')
const http = require('http').Server(app);
const axios = require('axios')
const io = require('socket.io')(http);

var indexRouter = require( __dirname+'/routes/index');
var loginRouter = require( __dirname+'/routes/login');
var usersRouter = require( __dirname+'/routes/users');
var apiRouter = require( __dirname+'/routes/api');

//required files
const mustache = require("mustache")

// To set functioning of mustachejs view engine
app.engine('html', function (filePath, options, callback) {
    fs.readFile(filePath, function (err, content) {
        if(err)
            return callback(err)
        let rendered = mustache.render(content.toString(), options);
        return callback(null, rendered)
    });
});

// Setting mustachejs as view engine
app.set('views', __dirname+'/public');
app.set('view engine','html');

app.use(helmet());
app.use(helmet.noCache())
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/login', loginRouter);
app.use('/users', usersRouter);
app.use('/api/v1', apiRouter);

const employeeService = require(__dirname+'/service/employee');
const slackmessageService = require(__dirname+'/service/slackmessage');

global.allClients = [];

global.users_connected = 0;

global.max_customers = 23;

global.current_customers = 0;

global.current_customers_waiting = 0;

global.max_customers_per_type = {
    accessories : 4,
    ios: 4,
    watch: 2,
    mac: 4,
    support: 2,
    repair: 2,
    escalation: 0
}

/*accessories : 4,
    ios: 3,
    watch: 2,
    mac: 3,
    support: 1,
    repair: 1*/

global.current_customers_per_type = {
    accessories : 0,
    ios: 0,
    watch: 0,
    mac: 0,
    support: 0,
    repair: 0,
    escalation: 0
}

global.role_needed = {
    accessories : 1,
    ios: 2,
    watch: 2,
    mac: 3,
    support: 1,
    repair: 4,
    escalation: 5
}

global.assignments = [];

readAssignments();

let isCheckingForEmployees = false;

function getRndInteger(min, max) {
    return Math.floor(Math.random() * (max - min) ) + min;
}

async function checkForAvailableEmployee() {

    isCheckingForEmployees = true
    setInterval( async function () {
        let all_av_empl = await employeeService.getAllAvailableEmployees();
        for (let i = 0; i < all_av_empl.length; i++) {
            delete all_av_empl[i].slack_channel
        }
        io.emit('num_available_employees', all_av_empl.length);
        io.emit('available_employees', all_av_empl);
        // Loop over all the pushed assignments

        let waiting_assignments = 0;
        let busy_assignments = 0;
        let people_store = 0;



        for (let i = 0; i < assignments.length; i++) {

            console.info("####################\n####################\nITERATION assignment\n####################\n####################\n\n")
            // Check if assignment is a waiting assignment
            if (assignments[i].state === "Wachten") {

                waiting_assignments++;

                // Get minimum role for this type
                let needed_role = role_needed[assignments[i].type];

                // Get max amount of customers for this type
                let max_customers_for_type = max_customers_per_type[assignments[i].type];

                let av_empl;
                console.log("\n\nassignments[i].type "+assignments[i].type+"\n\n")
                if (assignments[i].type === "escalation") {
                    av_empl = await employeeService.getAllAvailableEmployeesWithEscRole(needed_role);
                } else {
                    av_empl = await employeeService.getAllAvailableEmployeesWithRole(needed_role);
                }


                let customers_to_add = assignments[i].num_customers;

                let current_num_customers_for_type = current_customers_per_type[assignments[i].type];
                let max_num_customers_for_type = Number(max_customers_per_type[assignments[i].type]);
                let num_customers_for_type_if_added = Number(current_num_customers_for_type) + Number(customers_to_add);

                let max_total_customers = max_customers;
                let max_total_customers_if_added = Number(current_customers) + Number(customers_to_add);

                console.log("\nTYPE: "+assignments[i].type);
                console.log("\nmax_customers_per_type_FOR THIS "+JSON.stringify(max_customers_per_type[assignments[i].type]));
                console.log("\nmax_customers_per_type "+JSON.stringify(max_customers_per_type));

                console.log("customers_to_add "+customers_to_add)
                console.log("current_num_customers_for_type "+current_num_customers_for_type)
                console.log("max_num_customers_for_type "+max_num_customers_for_type)
                console.log("num_customers_for_type_if_added "+num_customers_for_type_if_added)
                console.log("max_total_customers "+max_total_customers)
                console.log("current_customers "+current_customers)
                console.log("max_total_customers_if_added "+max_total_customers_if_added)

                // 2 <= 1
                //
                console.log(num_customers_for_type_if_added <= max_num_customers_for_type)
                console.log(max_total_customers_if_added <= max_total_customers)
                if ( num_customers_for_type_if_added <= max_num_customers_for_type && max_total_customers_if_added <= max_total_customers) {
                    console.info("##########\nAssignment to check:\n\n" + JSON.stringify(assignments[i]))
                    all_av_empl = await employeeService.getAllAvailableEmployees();
                    for (let i = 0; i < all_av_empl.length; i++) {
                        delete all_av_empl[i].slack_channel
                    }
                    if (all_av_empl.length > 0) {
                        let bokje = getRndInteger(0, av_empl.length);
                        console.log(av_empl[bokje])
                        assignments[i].state = av_empl[bokje].name.first;
                        assignments[i].emp_id = av_empl[bokje].id;
                        console.log("current_customers_per_type[assignments[i].type]"+current_customers_per_type[assignments[i].type])
                        current_customers_per_type[assignments[i].type] = Number(current_customers_per_type[assignments[i].type]) + Number(assignments[i].num_customers);
                        console.log("current_customers_per_type[assignments[i].type]"+current_customers_per_type[assignments[i].type])
                        current_customers = Number(current_customers) + Number(assignments[i].num_customers) + 1;
                        console.info("##########\nAssignment to update:\n\n" + JSON.stringify(assignments[i]))
                        employeeService.changeEmployeeState(av_empl[bokje].id, 0);
                        if (assignments[i].type == "escalation") {
                            let lookedUpEscalation_to_employee = await employeeService.getEmployeeByID(assignments[i].escalation_to_employee);
                            assignments[i].escalation_to_employee = lookedUpEscalation_to_employee.name.first + " " + lookedUpEscalation_to_employee.name.last;
                        }
                        writeAssignments();
                        let message = "Nieuwe "+assignments[i].type_hr+" voor "+av_empl[bokje].name.first;
                        if (av_empl[bokje].slack_channel !== null || av_empl[bokje].slack_channel !== "") {
                            await slackmessageService.sendMessageToEmployeeByChannelID(av_empl[bokje].slack_channel, message)
                        }
                        io.emit('load_assignments', assignments);
                    }
                }

            } else {
                // assignment is busy

                busy_assignments++;

                people_store = people_store + Number(assignments[i].num_customers) + 1;

            }
        } // END assignments for loop
        all_av_empl = await employeeService.getAllAvailableEmployees();
        for (let i = 0; i < all_av_empl.length; i++) {
            delete all_av_empl[i].slack_channel
        }
        io.emit('num_customers_queue', waiting_assignments);
        io.emit('num_customers_store', busy_assignments);
        io.emit('num_people_store', people_store);
        io.emit('num_available_employees', all_av_empl.length);
        io.emit('available_employees', all_av_empl);
        io.emit('load_assignments', assignments);

    }, 5000)





}

async function sendNumAvailableEmpl() {
    let av_empl_all = await employeeService.getAllAvailableEmployees();
    for (let i = 0; i < av_empl_all.length; i++) {
        delete av_empl_all[i].slack_channel
    }
    io.emit('num_available_employees', av_empl_all.length);
    io.emit('available_employees', av_empl_all);
}

io.on('connection', (socket) => {

    console.log('a user connected');
    allClients.push(socket);

    io.emit('num_active_users', allClients.length);
    io.emit('num_assignments', assignments.length);
    io.emit('num_customers_queue', assignments.length);
    io.emit('num_customers_store', current_customers);
    io.emit('load_assignments', assignments);
    sendNumAvailableEmpl();

    socket.on('employee_state', (employeeData) => {

        employeeService.changeEmployeeState(employeeData.employee_id, employeeData.available);

        /*let aID = assignmentData[0].position;

        for(var i = 0, m = null; i < assignments.length; ++i) {
            if(assignments[i].position !== aID)
                continue;
            if (assignments[i].state === "Wachten") {
                current_customers = current_customers + Number(assignments[i].num_customers) + Number(1);
                current_customers_per_type[ assignments[i].type ] = Number(current_customers_per_type[ assignments[i].type ]) + Number(assignments[i].num_customers);
            }
            assignments[i].state = newEmployeeName;
            assignments[i].emp_id = assignmentData[0].emp_id;


            break;
        }*/
        console.log("Employee #"+employeeData.employee_id+" set to "+employeeData.available);
        socket.emit('employee_state', employeeData)
        sendNumAvailableEmpl();

    });

    socket.on('update_assignment', (assignmentData) => {
        console.log("UPDATE assignment")
        sendNumAvailableEmpl();
        console.log(assignmentData)

        // Change state to new employee name

        changeAssignment(assignmentData)

    });

    socket.on('new_assignment', (assignmentData) => {
        console.log("New assignment")
        console.log(assignmentData)
        //assignmentData.position = assignments.length+1;

        let ts = Date.now();
        assignmentData.position = ts+(assignments.length+1);

        assignments.push(assignmentData)
        writeAssignments();
        io.emit('new_assignment', assignmentData);
        io.emit('num_active_users', allClients.length);
        io.emit('num_customers_queue', assignments.length);
        io.emit('num_customers_store', current_customers);

        sendNumAvailableEmpl();
        if (!isCheckingForEmployees) {
            isCheckingForEmployees = true;
            checkForAvailableEmployee();
        }
    });

    socket.on('end_assignment', (assignmentData) => {
        console.log("END assignment")
        console.log(assignmentData)

        let pos_in_assignments = Number(assignmentData.assignment_position) - 1;
        let picked = assignments.find(o => o.position == assignmentData.assignment_position);

        let position_in_array = assignments.indexOf(picked)

        console.log("\nPOSITION TO END: "+position_in_array+"\n\n")

        console.log("TO END:\n+"+JSON.stringify(assignments[position_in_array]))
        if (assignments[position_in_array].state !== "Wachten") {
            current_customers = current_customers - Number(assignments[position_in_array].num_customers) - Number(1);
            current_customers_per_type[ assignments[position_in_array].type ] = Number(current_customers_per_type[ assignments[position_in_array].type ]) - Number(assignments[position_in_array].num_customers);
        }
        assignments.splice(position_in_array, 1)

        io.emit('num_active_users', allClients.length);
        io.emit('num_customers_queue', assignments.length);
        io.emit('num_customers_store', current_customers);
        sendNumAvailableEmpl();
        writeAssignments();
        io.emit('load_assignments', assignments);
    });

    socket.on('remove_assignment', (assignmentData) => {
        console.log("REMOVE assignment")
        console.log(assignmentData)
        let pos_in_assignments = Number(assignmentData.assignment_position) - 1;

        let picked = assignments.find(o => o.position == assignmentData.assignment_position);

        let position_in_array = assignments.indexOf(picked)

        console.log("\nPOSITION TO DELETE: "+position_in_array+"\n\n")

        console.log("TO DELETE:\n+"+JSON.stringify(assignments[position_in_array]))
        if (assignments[position_in_array].state !== "Wachten") {
            current_customers = current_customers - Number(assignments[position_in_array].num_customers) - Number(1);
            current_customers_per_type[ assignments[position_in_array].type ] = Number(current_customers_per_type[ assignments[position_in_array].type ]) - Number(assignments[position_in_array].num_customers);
        }
        assignments.splice(position_in_array, 1)
        //assignments.splice(Number(assignmentData.assignment_position - 1), 1)
        io.emit('num_active_users', allClients.length);
        io.emit('num_customers_queue', assignments.length);
        io.emit('num_customers_store', current_customers);
        sendNumAvailableEmpl();
        writeAssignments();
        io.emit('load_assignments', assignments);
    });

    socket.on('disconnect', () => {
        console.log('user disconnected');
        let i = allClients.indexOf(socket);
        allClients.splice(i, 1);
        io.emit('num_active_users', allClients.length);
    });
});

async function changeAssignment(assignmentData) {
    let newEmployeeObj = await employeeService.getEmployeeByID(assignmentData[0].emp_id);

    let newEmployeeName = newEmployeeObj.name.first;

    let oldEmployeeID = assignmentData.emp_id

    let oldEmployeeObj = await employeeService.getEmployeeByID(oldEmployeeID)

    let aID = assignmentData[0].position;

    for(var i = 0, m = null; i < assignments.length; ++i) {
        if(assignments[i].position !== aID)
            continue;
        if (assignments[i].state === "Wachten") {
            current_customers = current_customers + Number(assignments[i].num_customers) + Number(1);
            current_customers_per_type[ assignments[i].type ] = Number(current_customers_per_type[ assignments[i].type ]) + Number(assignments[i].num_customers);
        }
        assignments[i].state = newEmployeeName;
        assignments[i].emp_id = assignmentData[0].emp_id;

        let message = "Overgenomen "+assignments[i].type_hr+" voor "+newEmployeeObj.name.first;
        let Oldmessage = "Je "+assignments[i].type_hr+" is overgenomen. Je inzet is niet meer nodig ";

        if (oldEmployeeObj.slack_channel !== null || oldEmployeeObj.slack_channel !== "") {
            await slackmessageService.sendMessageToEmployeeByChannelID(oldEmployeeObj.slack_channel, messageOld)
        }
        if (newEmployeeObj.slack_channel !== null || newEmployeeObj.slack_channel !== "") {
            await slackmessageService.sendMessageToEmployeeByChannelID(newEmployeeObj.slack_channel, message)
        }
        break;
    }


    writeAssignments();
    io.emit('load_assignments', assignments);
    io.emit('num_active_users', allClients.length);
    io.emit('num_customers_queue', assignments.length);
    io.emit('num_customers_store', current_customers);
}

function writeAssignments() {
    fs.writeFile('assignmentData.json', JSON.stringify(assignments), function (err) {
        if (err) return console.log(err);
        console.log('SAVED THE ASSIGNMENTS');
    });
}

function readAssignments() {
    fs.readFile('assignmentData.json', 'utf8', function (err,data) {
        if (err) {
            return console.log(err);
        }
        console.log("\n\n###############\nREAD ASSIGNMENT FILE");
        console.log(data);
        console.log("\n\n######## FILE READED \n\n###############\n");
        let assignments_file = JSON.parse(data);
        console.log("Length "+assignments_file.length)


        // print all databases
        assignments_file.forEach(db => {
            assignments.push(db);
            if (db.state !== "Wachten") {
                current_customers_per_type[db.type] = Number(current_customers_per_type[db.type]) + Number(db.num_customers);
                current_customers = Number(current_customers) + Number(db.num_customers) + 1;
            }

            console.log("pushed data to assignments array\n\n");

            console.log("TO add: "+db+"\n\n");
            console.log("current object: "+JSON.stringify(assignments)+"\n\n");

        });

        io.emit('load_assignments', assignments);

        console.log("\n\n######## FILE saved to var \n\n###############\n");

        if (assignments.length > 0 ) {
            checkForAvailableEmployee();
            isCheckingForEmployees = true
        }
    });
}

http.listen(process.env.PORT || 3000);

console.info("Running");