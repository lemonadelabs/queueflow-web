$( document ).ready( function () {
    $("*[data-view-open]").click( function () {
        let selector = '.view[data-view-name="'+$(this).attr("data-view-open")+'"]';
        $(".view").hide();
        $(selector).show();
    });
});