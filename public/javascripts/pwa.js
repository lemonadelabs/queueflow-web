// Copyright 2020 Bas Kasemir
var allScreen = false;
var plusModel = false;

$( document ).ready( function () {
    if (!window.navigator.standalone && profile !== "dev") {
        isPWA = true;
        $(".pwa").show();
        $(".view, .tabs-bar, .prompt-message").hide();
    }


    let viewportWidth = $(window).width();
    let viewportHeight = $(window).height();

    //alert("viewportWidth: "+viewportWidth+"\n"+"viewportHeight: "+viewportHeight);

    switch (viewportWidth) {

        case 375:
            if (viewportHeight < 750) {
                allScreen = false;
                plusModel = false;
            } else {
                allScreen = true;
                plusModel = false;
            }
            break;

        case 414:
            allScreen = true;
            plusModel = false;
            break;

        case 390:
            allScreen = true;
            plusModel = false;
            break;

        case 428:
            allScreen = true;
            plusModel = false;
            break;

        case 360:
            allScreen = true;
            plusModel = false;
            break;

        default:
            allScreen = false;
            plusModel = false;
            break;
    }

    if (!allScreen) {
        $(".tabs-bar").css('height', '50px');
    }

    $(window).resize( modelHasAllScreen() );
});

function modelHasAllScreen() {
    return
}