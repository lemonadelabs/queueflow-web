$(document).ready( function () {
    console.log("icare loaded")
    fetchDevices();
});

function fetchDevices() {

    console.log("fetch dev loaded")
    $("#devices-list").empty();
    let devicesData = (function () {
        let json = null;
        $.ajax({
            'async': false,
            'global': false,
            'url': "data/devices.json",
            'dataType': "json",
            'success': function (data) {
                console.log(data)
                json = JSON.parse(data);
                console.log(json)

            }
        });
        return json;
    })();

    for (let i = 0; i < devicesData.length; i++) {
        console.log("foreach dev loaded")
        $("#devices-list").append('<button>' + devicesData[i] + '</button>');
    }


}
