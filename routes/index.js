const express = require('express');
const router = express.Router();
const bodyParser = require("body-parser");

const employeeService = require('../service/employee');

const requireAuth = require('../middleware/require-auth.js');

function consolelog(req, res) {
  console.log("efsgdsfgdfgsfgdfsgdsfgdxfgdfxgdfgh")
}

/* GET home page. */
router.get('/app', async function(req, res, next) {
  let employees = await employeeService.getAllEmployees()

  let attributes = {}
  attributes.employees = employees;

  res.render('blackfriday', attributes);
});

/* GET home page. */
router.get('/narrowcasting', async function(req, res, next) {
  let employees = await employeeService.getAllEmployees()

  let attributes = {}
  attributes.employees = employees;

  res.render('narrowcasting', attributes);
});


module.exports = router;
