var mysql = require('mysql');
var poolconnection  = mysql.createPool({
    connectionLimit : process.env.DB_CONNECTION_LIMIT,
    host     : process.env.DB_HOST,
    user     : process.env.DB_USER,
    password : process.env.DB_PASS,
    database : process.env.DB_DB,
});

class Database {
    constructor() {
        this.connection = poolconnection
    }
    query( sql, args ) {
        return new Promise( ( resolve, reject ) => {
            this.connection.query( sql, args, ( err, rows ) => {
                if ( err )
                    return reject( err );
                resolve( rows );
            } );
        } );
    }
    release() {
        return new Promise( ( resolve, reject ) => {
            this.connection.release( err => {
                if ( err )
                    return reject( err );
                resolve();
            } );
        } );
    }
    close() {
        return new Promise( ( resolve, reject ) => {
            this.connection.end( err => {
                if ( err )
                    return reject( err );
                resolve();
            } );
        } );
    }
    testSQLInjection(param) {
        return new RegExp("/[\\t\\r\\n]|(--[^\\r\\n]*)|(\\/\\*[\\w\\W]*?(?=\\*)\\*\\/)/gi").test(param);
    }
}

module.exports = new Database()