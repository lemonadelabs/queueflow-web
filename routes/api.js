const express = require('express');
const router = express.Router();
const bodyParser = require("body-parser");
const axios = require('axios');

const slackmessageService = require('../service/slackmessage');

const employeeService = require('../service/employee');

const requireAuth = require('../middleware/require-auth.js');

/* GET home page. */
router.post('/employee/change/available/:id/:state', async function(req, res, next) {
    let empID = req.params.id;
    let empState = req.params.state;

    let status = await employeeService.changeEmployeeState(empID, empState);

    res.json({affectedRows: status});
});

/* GET home page. */
router.get('/employee/get/available/all', async function(req, res, next) {
    let employee = await employeeService.getAllAvailableEmployees();
    if (employee.length < 1) {
        res.json({status: "employees not found"});
    } else {
        for (let i = 0; i < employee.length; i++) {
            delete employee[i].employee_code;
            delete employee[i].slack_channel;
            delete employee[i].enabled;
        }

        res.json(employee);

    }
});

/* GET home page. */
router.get('/employee/get/available/:id', async function(req, res, next) {
    let empID = req.params.id;
    let employee = await employeeService.getEmployeeByID(empID);
    if (employee.length < 1) {
        res.json({status: "employee not found"});
    } else {
        res.json({available: employee.available});
    }
});

/* GET home page. */
router.get('/employee/got/assignment/:id', async function(req, res, next) {
    let empID = req.params.id;


    console.log("assignments.length "+assignments.length)
    console.log("empID "+empID)

    if (empID < 1) {
        res.json({"error": "Id has not a valid value"});
        return
    }

    let foundAssignment = false;
    let assignmentPosition;
    for(var i = 0, m = null; i < assignments.length; ++i) {
        if(Number(assignments[i].emp_id) !== Number(empID))
            continue;
        foundAssignment = true;
        assignmentPosition = i;
        console.log("assignments[i].emp_id "+assignments[i].emp_id)
        break;
    }

    if (foundAssignment) {
        res.json(assignments[assignmentPosition]);
        return
    } else {
        res.json({"error": "No assignment found for user "+empID})
        return
    }

});

/* POST for subscribing slackbot */
//https://queueflow.nl/api/v1/employee/subscribe
router.post('/employee/subscribe', async function(req, res, next) {
    /*var book_id = req.body.id;
    var bookName = req.body.token;*/
    console.log(JSON.stringify(req.body))

    let channelID = req.body.channel_id;
    console.log(channelID)
    let commandText = req.body.text.split(" ");
    let storeName = commandText[0];
    let employeeCode = commandText[1];

    // send to store's API endpoint

    axios
        .post('https://'+storeName+'.queueflow.nl/api/v1/employee/subscribe', {
            text: employeeCode
        })
        .then(res => {
            console.log(`statusCode: ${res.statusCode}`)
            console.log(res.data)
            return true
        })
        .catch(error => {
            console.error(error)
            return false
        })

    res.json({status: true});

    /*



    let employeeDetails = await employeeService.getEmployeeByEmployeecode(employeeCode);

    let newly_registered = false;
    let sendMessage;

    console.log("employeeDetails.slack_channel "+employeeDetails.slack_channel)
    if (employeeDetails.slack_channel === null || employeeDetails.slack_channel === "" || employeeDetails.slack_channel === "null") {
        // Slack channel is not set yet.

        let changeEmployee = await employeeService.changeEmployeeSlackchannelByCode(employeeCode, channelID);

        console.log("changeEmployee: "+changeEmployee)
        if (changeEmployee === 1 ) {
            let message = "Je staat nu geregistreerd voor het ontvangen van meldingen voor werknemer " + employeeCode;
            sendMessage = await slackmessageService.sendMessageToEmployeeByChannelID(channelID, message)

        } else {
            sendMessage = "WARNING: Subscription for "+employeeCode+" has not been changed.";
        }

        res.json({status: sendMessage});
        return

    } else {

        if (channelID === employeeDetails.slack_channel) {
            sendMessage = "Already registered by this Slack account";
        } else {
            sendMessage = "Already registered by another account";
        }

        res.json({status: sendMessage});
        return
    }*/



});

/* POST for subscribing slackbot */
//https://queueflow.nl/api/v1/employee/subscribe
router.post('/employee/unsubscribe', async function(req, res, next) {
    /*var book_id = req.body.id;
    var bookName = req.body.token;*/
    console.log(JSON.stringify(req.body))

    let channelID = req.body.channel_id;
    console.log(channelID)
    let employeeCode = req.body.text;

    let employeeDetails = await employeeService.getEmployeeByEmployeecode(employeeCode);

    let newly_registered = false;
    let sendMessage;

    console.log("employeeDetails.slack_channel "+employeeDetails.slack_channel)
    if (employeeDetails.slack_channel === null || employeeDetails.slack_channel === "") {
        // Slack channel is not set yet.
        sendMessage = "You're not subscribed yet"
        res.json({status: sendMessage});
        return

    } else {

        if (channelID === employeeDetails.slack_channel) {

            let changeEmployee = await employeeService.changeEmployeeSlackchannelByCode(employeeCode, "");
            console.log("changeEmployee: "+changeEmployee)
            if (changeEmployee === 1 ) {
                let message = "Je staat niet langer geregistreerd voor het ontvangen van meldingen voor werknemer " + employeeCode;
                sendMessage = await slackmessageService.sendMessageToEmployeeByChannelID(channelID, message)
            } else {
                sendMessage = "WARNING: Subscription for "+employeeCode+" has not been changed.";
            }

            res.json({status: sendMessage});
            return

        }

    }


});

module.exports = router;
