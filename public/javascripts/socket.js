var socket = io();

socket.on('num_active_users', function(msg){
    $('*[data-show-active_users]').text(msg);
});

socket.on('num_available_employees', function(msg){
    $('*[data-show-available_employees]').text(msg);
});

socket.on('num_customers_queue', function(msg){
    $('*[data-show-customers_queue]').text(msg);
});

socket.on('num_customers_store', function(msg){
    $('*[data-show-customers_store]').text(msg);
});

socket.on('num_people_store', function(msg){
    $('*[data-show-people_store]').text(msg);
});