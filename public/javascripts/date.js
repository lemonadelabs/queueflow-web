// Construct and format the current date. Set as global variable so it can be used in other JS files
var d = new Date,
day = d.getDate() < 10 ? "0" + d.getDate() : d.getDate(),
month = ((d.getMonth()+1) < 10 ? "0" + (d.getMonth()+1) : (d.getMonth()+1)),
year = d.getFullYear();

$( document ).ready( function() {
    
    $( "*[data-date]" ).each(function() {
        var datapart = $( this ).attr( "data-date" );
        
        var elementType = $(this).prop('nodeName');
        
        console.log(datapart);
        
        if (elementType == "INPUT") {
            $( this ).val(Date.today().toString(datapart));
        } else {
            $( this ).text(Date.today().toString(datapart));
        }
        
    });
    
});