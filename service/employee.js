const db = require('../datasource/db');
var Employee = require('../model/employee');

module.exports.getEmployeeByEmployeecode = async function(code) {

    const sqlquery = "SELECT * FROM employees WHERE employee_code='" + code + "'";
    // node native promisify

    let employee = new Employee()
    try {
        employee = await db.query( sqlquery ).then( rows => {
            // do something with the result
            employee.id = rows[0].id;
            employee.name.first = rows[0].first_name;
            employee.name.last = rows[0].last_name;
            employee.role = rows[0].role;
            employee.employee_code = rows[0].employee_code;
            employee.enabled = rows[0].enabled;
            employee.available = rows[0].available;
            employee.slack_channel = rows[0].slack_channel;
            employee.escalation = rows[0].escalation;
            return employee
        } );
        return employee
    } catch (e) {
        console.error("[getEmployeeByEmployeecode] : " + "ERROR RETRIEVING EMPLOYEE")
        console.error("exception: "+e)
    }

    // the following code is executed *before* the query is executed

}

module.exports.getEmployeeByID = async function(id) {

    const sqlquery = "SELECT * FROM employees WHERE id='" + id + "'";
    // node native promisify

    let employee = new Employee()
    try {
        employee = await db.query( sqlquery ).then( rows => {
            // do something with the result
            employee.id = rows[0].id;
            employee.name.first = rows[0].first_name;
            employee.name.last = rows[0].last_name;
            employee.role = rows[0].role;
            employee.employee_code = rows[0].employee_code;
            employee.enabled = rows[0].enabled;
            employee.available = rows[0].available;
            employee.slack_channel = rows[0].slack_channel;
            employee.escalation = rows[0].escalation;
            return employee
        } );
        return employee
    } catch (e) {
        console.error("[getEmployeeByID] : " + "ERROR RETRIEVING EMPLOYEE")
        console.error("exception: "+e)
    }

    // the following code is executed *before* the query is executed

}


module.exports.getAllEmployees = async function() {

    const sqlquery = "SELECT * FROM employees";
    // node native promisify

    try {
        let employees = await db.query( sqlquery ).then( rows => {
            let employeeList = []
            for (let i = 0; i < rows.length; i++) {
                let employee = null
                employee = new Employee()
                employee.id = rows[i].id;
                employee.name.first = rows[i].first_name;
                employee.name.last = rows[i].last_name;
                employee.role = rows[i].role;
                employee.employee_code = rows[i].employee_code;
                employee.enabled = rows[i].enabled;
                employee.available = rows[i].available;
                employee.slack_channel = rows[i].slack_channel;
                employee.escalation = rows[i].escalation;
                employeeList.push(employee)
            }
            return employeeList
        } );

        return employees
    } catch (e) {
        console.error("[getAllEmployees] : " + "ERROR RETRIEVING USER")
        console.error("exception: "+e)
    }

    // the following code is executed *before* the query is executed

}

module.exports.getAllAvailableEmployees = async function() {

    const sqlquery = "SELECT * FROM employees WHERE available = 1";
    // node native promisify

    try {
        let employees = await db.query( sqlquery ).then( rows => {
            let employeeList = []
            for (let i = 0; i < rows.length; i++) {
                let employee = null
                employee = new Employee()
                employee.id = rows[i].id;
                employee.name.first = rows[i].first_name;
                employee.name.last = rows[i].last_name;
                employee.role = rows[i].role;
                employee.employee_code = rows[i].employee_code;
                employee.enabled = rows[i].enabled;
                employee.available = rows[i].available;
                employee.slack_channel = rows[i].slack_channel;
                employee.escalation = rows[i].escalation;
                employeeList.push(employee)
            }
            return employeeList
        } );

        return employees
    } catch (e) {
        console.error("[getAllAvailableEmployees] : " + "ERROR RETRIEVING USER")
        console.error("exception: "+e)
    }

    // the following code is executed *before* the query is executed

}

module.exports.getAllAvailableEmployeesWithRole = async function(role) {

    const sqlquery = "SELECT * FROM employees WHERE role >= "+role+" AND available = 1  AND escalation = 0";
    // node native promisify

    console.log(sqlquery)

    try {
        let employees = await db.query( sqlquery ).then( rows => {
            let employeeList = []
            for (let i = 0; i < rows.length; i++) {
                let employee = null
                employee = new Employee()
                employee.id = rows[i].id;
                employee.name.first = rows[i].first_name;
                employee.name.last = rows[i].last_name;
                employee.role = rows[i].role;
                employee.employee_code = rows[i].employee_code;
                employee.enabled = rows[i].enabled;
                employee.available = rows[i].available;
                employee.slack_channel = rows[i].slack_channel;
                employee.escalation = rows[i].escalation;
                employeeList.push(employee)
            }
            return employeeList
        } );

        return employees
    } catch (e) {
        console.error("[getAllAvailableEmployeesWithRole] : " + "ERROR RETRIEVING USER")
        console.error("exception: "+e)
    }

    // the following code is executed *before* the query is executed

}

module.exports.getAllAvailableEmployeesWithEscRole = async function(role) {

    const sqlquery = "SELECT * FROM employees WHERE role >= "+role+" AND available = 1 AND escalation = 1";
    // node native promisify

    console.log(sqlquery)

    try {
        let employees = await db.query( sqlquery ).then( rows => {
            let employeeList = []
            for (let i = 0; i < rows.length; i++) {
                let employee = null
                employee = new Employee()
                employee.id = rows[i].id;
                employee.name.first = rows[i].first_name;
                employee.name.last = rows[i].last_name;
                employee.role = rows[i].role;
                employee.employee_code = rows[i].employee_code;
                employee.enabled = rows[i].enabled;
                employee.available = rows[i].available;
                employee.escalation = rows[i].escalation;
                employeeList.push(employee)
            }
            return employeeList
        } );

        return employees
    } catch (e) {
        console.error("[getAllAvailableEmployeesWithRole] : " + "ERROR RETRIEVING USER")
        console.error("exception: "+e)
    }

    // the following code is executed *before* the query is executed

}

module.exports.changeEmployeeState = async function(id, state) {

    const sqlquery = "UPDATE employees SET available = "+state+" WHERE id = "+id;
    // node native promisify

    try {
        let employees = await db.query( sqlquery ).then( rows => {
            return rows.changedRows
        } );

        return employees
    } catch (e) {
        console.error("[changeEmployeeState] : " + "ERROR UPDATING USER")
        console.error("exception: "+e)
    }

    // the following code is executed *before* the query is executed

}

module.exports.changeEmployeeSlackchannelByCode = async function(code, channelID) {

    const sqlquery = "UPDATE employees SET slack_channel = '"+channelID+"' WHERE employee_code = "+code;
    // node native promisify

    try {
        let employees = await db.query( sqlquery ).then( rows => {
            return rows.changedRows
        } );

        return employees
    } catch (e) {
        console.error("[changeEmployeeState] : " + "ERROR UPDATING changeEmployeeSlackchannelByCode")
        console.error("exception: "+e)
    }

    // the following code is executed *before* the query is executed

}