$( document ).ready( function () {
    $(".tab").click( function () {
        $(".tab").removeClass("selected");
        $(this).addClass("selected");
        let selector = '.view[data-view-name="'+$(this).attr("data-view-open")+'"]';
        $(".view").hide();
        $(selector).show();
    });
});