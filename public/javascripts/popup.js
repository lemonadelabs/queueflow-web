/* Copyright 2020 Bas Kasemir*/
window.popup = (function() {
    function popup(els) {
        if (typeof jQuery == 'undefined') {
            console.error("JQuery is not loaded. Please include it in a line before the Cobalt script.")
        }
    }
    var popup = {
        make: function(options) {
            if (options == null) {
                console.error("No options specified");
                return
            }
            destroy();
            make(options)
        },
        destroy: function() {
            destroy()
        }
    };
    return popup
}());
function make(options) {
    let title = options.title;
    let data = options.data;
    let buttonText = options.buttonText;
    let cancelButtonText = options.cancelButtonText;
    let showCancelButton = options.showCancelButton;
    let showButton = options.showButton;

    let buttonsData;
    let buttonData;
    let cancelButtonData;
    let showButtonCheck = showButton === true && showButton !== undefined;
    let showCancelButtonCheck = showCancelButton === true && showCancelButton !== undefined;
    let textButtonCheck = buttonText != null && buttonText !== "";

    if (true) {
        if (buttonText == null || buttonText === "" || buttonText === undefined) {
            buttonText = "OK"
        }
        if (cancelButtonText == null || cancelButtonText === "") {
            cancelButtonText = "Cancel"
        }
        buttonData = `<div id="popup-ok"><h5 class="blue-text">` + buttonText + `</h5></div>`;
        if (showCancelButtonCheck) {
            cancelButtonData = `<div id="popup-cancel"><h5 class="blue-text">` + cancelButtonText + `</h5></div>`
        } else {
            cancelButtonData = ""
        }
        buttonsData = cancelButtonData + buttonData;
    } else {
        buttonsData = `<div id="popup-ok"><h5 class="blue-text">OK</h5></div>`;
    }
    $("body").append(`
<div class="pop-up-glasspane"></div>
<div class="pop-up">
<div class="text-container">
<h5>`+title+`</h5>
<p>`+data+`</p>
</div>
<div class="buttons-container">
`+buttonsData+`</div>`
    );
    $('html').addClass("disablescroll");
    $("#popup-cancel").on('click', function() {
        destroy()
    });
    $("#popup-ok").on('click', function() {
        options.callbackFunction()
    })
}
function destroy() {
    $('div[class="pop-up"], div[class="pop-up-glasspane"]').remove();
    $('html').removeClass("disablescroll")
}
/*$(document).ready(function() {
    $("head").append('<link href="//basmonkey.nl/api/cobalt/cobalt.min.css" rel="stylesheet" type="text/css">')
});*/

