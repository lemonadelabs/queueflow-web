var Employee = require('../service/employee');
const axios = require('axios');

module.exports.sendMessageToEmployeeByChannelID = async function( channelID , message) {

    let config = {
        headers: {
            Authorization: 'Bearer xoxb-1539358571120-1515754581715-JTiLdTo0X0Tw9jgmF8JEqk9R',
        }
    }

    axios
        .post('https://slack.com/api/chat.postMessage', {
            channel: channelID,
            text: message
        }, config)
        .then(res => {
            console.log(`statusCode: ${res.statusCode}`)
            console.log(res.data)
            return true
        })
        .catch(error => {
            console.error(error)
            return false
        })

}
